import numpy as np
from sklearn.mixture import GaussianMixture as GMM

from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.optimizers import Adam

from tqdm import tqdm
import matplotlib
matplotlib.use('Agg')

from pylab import plot, clf, savefig, legend

lsdim=128   #latent space dimensionality
opt = Adam(lr=1e-4)
dopt = Adam(lr=1e-3)

def sample_from_gmm(K,D,N):
 
    gmm= GMM(K, covariance_type = 'diag')

    #Dummy fit to bypass the check before sampling
    gmm.fit(np.random.rand(10,D))

    gmm.means_ = np.random.rand(K,D)*20
    gmm.covariances_ = np.random.rand(K,D)
    wts = np.random.rand(K)

    print gmm.means_

    gmm.weights_ = wts/wts.sum()

    print "wts=",gmm.weights_

    X= gmm.sample(N)
    return X

def make_generator(ndim=2):

    model=Sequential()

    model.add(Dense(64,input_shape=(lsdim,),activation='relu'))
    model.add(Dense(32,activation='relu'))
    model.add(Dense(16,activation='relu'))
    model.add(Dense(8,activation='relu'))
    model.add(Dense(ndim,activation='linear'))

    return model

def make_discriminator(ndim=2):
    
    model=Sequential()

    model.add(Dense(16,input_shape=(ndim,),activation='relu'))
    model.add(Dense(32,activation='relu'))
    model.add(Dense(64,activation='relu'))
    model.add(Dense(32,activation='relu'))
    model.add(Dense(16,activation='relu'))
    model.add(Dense(2,activation='softmax'))

    return model

# Iterative training modules
def make_trainable(net, val):
    net.trainable = val
    for l in net.layers:
        l.trainable = val



gen = make_generator()
disc = make_discriminator()
disc.compile(loss='categorical_crossentropy',optimizer=dopt)

#Stack generator and discriminator together
gen_disc = Sequential()
gen_disc.add(gen)
gen_disc.add(disc)

gen_disc.compile(loss='categorical_crossentropy',optimizer=opt)

print gen_disc.summary()

ntrain=10000
#Sample data from a gmm
XT, rho= sample_from_gmm(K=2,D=2,N=ntrain)       #rho is the mixture id

#Pre-train the discriminator
noise_gen = np.random.uniform(0,1,size=(XT.shape[0],lsdim))
gen_data = gen.predict(noise_gen)

X = np.concatenate((XT, gen_data))
n = XT.shape[0]
y = np.zeros([2*n,2])

y[:n,1]=1
y[n:,0]=1

make_trainable(gen,False)
make_trainable(disc, True)
disc.fit(X,y, epochs=1, batch_size=128)
y_hat = disc.predict(X)

# Measure accuracy of pre-trained discriminator network
print "initial disc. acc. =", np.equal(np.argmax(y_hat,axis=1),np.argmax(y,axis=1)).mean()

# set up loss storage vector
losses = {"d":[], "g":[]}

def train_for_n(nb_epoch=5000, BATCH_SIZE=32):

    for e in tqdm(range(nb_epoch)):  
        
        #Make generative data

        data_batch = XT [np.random.randint(0,XT.shape[0], size= BATCH_SIZE)]

        noise_gen = np.random.uniform(0,1,size=(BATCH_SIZE,lsdim))
        gen_data = gen.predict(noise_gen)

        X = np.concatenate((data_batch, gen_data))
        y = np.zeros([2*BATCH_SIZE,2])
        y[:BATCH_SIZE,1] = 1
        y[BATCH_SIZE:,0] = 1

        make_trainable(disc,True)
        d_loss  = disc.train_on_batch(X,y)
        losses["d"].append(d_loss)


        # train Generator-Discriminator stack on input noise to non-generated output class
        noise_tr = np.random.uniform(0,1,size=[BATCH_SIZE,lsdim])
        y2 = np.zeros([BATCH_SIZE,2])
        y2[:,1] = 1
        
        make_trainable(disc,False)
        make_trainable(gen,True)
        g_loss = gen_disc.train_on_batch(noise_tr, y2 )
        losses["g"].append(g_loss)

train_for_n(6000,32)

# Train for 2000 epochs at reduced learning rates
opt.lr=1e-5
dopt.lr=1e-4
train_for_n(nb_epoch=2000, BATCH_SIZE=32)

# Train for 2000 epochs at reduced learning rates
opt.lr=1e-6
dopt.lr=1e-5
train_for_n(nb_epoch=2000, BATCH_SIZE=32)


clf()
plot(losses['g'])
plot(losses['d'])
legend('gen','disc')
savefig('imgs/losses.png')


clf()
noise_gen = np.random.uniform(0,1,size=(XT.shape[0],lsdim))
gen_data = gen.predict(noise_gen)
plot(XT[:,0],XT[:,1],'k.')
plot(gen_data[:,0],gen_data[:,1],'r.')
savefig('imgs/gmm.png')
