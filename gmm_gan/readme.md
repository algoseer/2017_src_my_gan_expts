Generate data from a GMM and try to learn the distribution using a GAN to demonstrate the mode collapse problem

based on the Keras MNIST GAN example

![gmm](./gmm2.png)
GMM true and estimated distributions (K=4, D=2)
![losses](./losses2.png)
Generator and Discriminator loss functions
